From: Aymeric Agon-Rambosson <aymeric.agon@yandex.com>
Date: Thu, 18 Aug 2022 10:25:32 +0200
Subject: Drop references to external pages and debianise
Forwarded: not-needed

--- a/README.org
+++ b/README.org
@@ -107,7 +107,7 @@ indicates a category of =buffer=. Embark
 /type/ of a target for actions, and by default when category metadata
 is present it is taken to be the type of minibuffer completion
 candidates when used as targets. Emacs commands often do not set
-useful category metadata so the [[https://github.com/minad/marginalia][Marginalia]] package, which supplies
+useful category metadata so the Marginalia (~apt install elpa-marginalia~) package, which supplies
 this missing metadata, is highly recommended for use with Embark.
 
 Embark's default configuration has actions for the following target
@@ -183,11 +183,11 @@ sets:
   Ibuffer buffer; and if they are packages you get a buffer in
   package menu mode.
 
-  If you use the grepping commands from the [[https://github.com/minad/consult/][Consult]] package,
+  If you use the grepping commands from the Consult (~apt install elpa-consult~) package,
   =consult-grep=, =consult-git-grep= or =consult-ripgrep=, then you should
   install the =embark-consult= package, which adds support for exporting a
   list of grep results to an honest grep-mode buffer, on which you can
-  even use [[https://github.com/mhayashi1120/Emacs-wgrep][wgrep]] if you wish.
+  even use wgrep (~apt install elpa-wgrep~) if you wish.
 
 When in doubt choosing between exporting and collecting, a good rule
 of thumb is to always prefer =embark-export= since when an exporter to a
@@ -275,21 +275,18 @@ switch to =find-file=.
 
 * Quick start
 
-The easiest way to install Embark is from GNU ELPA, just run =M-x
-package-install RET embark RET=. (It is also available on MELPA.) It is
-highly recommended to also install [[https://github.com/minad/marginalia][Marginalia]] (also available on GNU
-ELPA), so that Embark can offer you preconfigured actions in more
+The easiest way to install Embark is ~apt install elpa-embark~. It is
+highly recommended to also install Marginalia (~apt install elpa-marginalia~),
+so that Embark can offer you preconfigured actions in more
 contexts. For =use-package= users, the following is a very reasonable
 starting configuration:
 
 #+begin_src emacs-lisp
   (use-package marginalia
-    :ensure t
     :config
     (marginalia-mode))
 
   (use-package embark
-    :ensure t
 
     :bind
     (("C-." . embark-act)         ;; pick some comfortable binding
@@ -305,13 +302,12 @@ starting configuration:
 
     ;; Hide the mode line of the Embark live/completions buffers
     (add-to-list 'display-buffer-alist
-                 '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
-                   nil
-                   (window-parameters (mode-line-format . none)))))
+		 '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
+		   nil
+		   (window-parameters (mode-line-format . none)))))
 
-  ;; Consult users will also want the embark-consult package.
+  ;; Consult users will also want the included embark-consult package.
   (use-package embark-consult
-    :ensure t ; only need to install it, embark loads it after consult if found
     :hook
     (embark-collect-mode . consult-preview-at-point-mode))
 #+end_src
@@ -353,9 +349,9 @@ Embark needs to know what your minibuffe
 to be the list of candidates and which one is the current candidate.
 Embark works out of the box if you use Emacs's default tab completion,
 the built-in =icomplete-mode= or =fido-mode=, or the third-party packages
-[[https://github.com/minad/vertico][Vertico]], [[https://github.com/raxod502/selectrum/][Selectrum]] or [[https://github.com/abo-abo/swiper][Ivy]].
+Vertico (~apt install elpa-vertico~), [[https://github.com/raxod502/selectrum/][Selectrum]] or Ivy (~apt install elpa-ivy~).
 
-If you are a [[https://emacs-helm.github.io/helm/][Helm]] or [[https://github.com/abo-abo/swiper][Ivy]] user you are unlikely to want Embark since
+If you are a Helm (~apt install elpa-helm~) or Ivy user you are unlikely to want Embark since
 those packages include comprehensive functionality for acting on
 minibuffer completion candidates. (Embark does come with Ivy
 integration despite this.)
@@ -400,7 +396,7 @@ Embark comes with the following indicato
   lazily highlights all occurrences of that symbol in the current
   buffer, like isearch; also on by default.
 
-Users of the popular [[https://github.com/justbur/emacs-which-key][which-key]] package may prefer to use the
+Users of the popular which-key (~apt install elpa-which-key~) package may prefer to use the
 =embark-which-key-indicator= from the [[https://github.com/oantolin/embark/wiki/Additional-Configuration#use-which-key-like-a-key-menu-prompt][Embark wiki]]. Just copy its
 definition from the wiki into your configuration and customize the
 =embark-indicators= user option to exclude the mixed and verbose
@@ -657,7 +653,7 @@ similar situations where the easiest opt
 
 *** New minibuffer target example - tab-bar tabs
 
-As an example, take the new [[https://www.gnu.org/software/emacs/manual/html_node/emacs/Tab-Bars.html][tab bars]] from Emacs 27. I'll explain how
+As an example, take the new [[info:emacs#Tab Bars][tab bars]] from Emacs 27. I'll explain how
 to configure Embark to offer tab-specific actions when you use the
 tab-bar-mode commands that mention tabs by name. The configuration
 explained here is now built-in to Embark (and Marginalia), but it's
@@ -675,7 +671,7 @@ for example, do have metadata indicating
 do not, unfortunately, and I will describe a couple of ways to deal
 with this.
 
-Maybe the easiest thing is to configure [[https://github.com/minad/marginalia][Marginalia]] to enhance those
+Maybe the easiest thing is to configure Marginalia to enhance those
 commands. All of the =tab-bar-*-tab-by-name= commands have the words
 "tab by name" in the minibuffer prompt, so you can use:
 
@@ -708,7 +704,7 @@ metadata looks:
 As you can see, the built-in support for setting the category
 meta-datum is not very easy to use or pretty to look at. To help with
 this I recommend the =consult--read= function from the excellent
-[[https://github.com/minad/consult/][Consult]] package. With that function we can rewrite the command as
+Consult package. With that function we can rewrite the command as
 follows:
 
 #+begin_src emacs-lisp
@@ -921,7 +917,7 @@ included in the list =embark-indicators=
 
 * Embark, Marginalia and Consult
 
-Embark cooperates well with the [[https://github.com/minad/marginalia][Marginalia]] and [[https://github.com/minad/consult][Consult]] packages.
+Embark cooperates well with the Marginalia (~apt install elpa-marginalia~) and Consult (~apt install elpa-consult~) packages.
 Neither of those packages is a dependency of Embark, but both are
 highly recommended companions to Embark, for opposite reasons:
 Marginalia greatly enhances Embark's usefulness, while Embark can help
@@ -989,7 +985,7 @@ The =embark-consult= package provides th
 - You can export from any of the Consult asynchronous search commands,
   =consult-grep=, =consult-git-grep=, or =consult-ripgrep= to get a
   =grep-mode= buffer. Here too you can use =next-error= and =previous-error=
-  to navigate among matches, and, if you install the [[http://github.com/mhayashi1120/Emacs-wgrep/raw/master/wgrep.el ][wgrep]] package,
+  to navigate among matches, and, if you install the wgrep (~apt install elpa-wgrep~) package,
   you can use it to edit the matches in place.
 
 In both cases, pressing =g= will rerun the Consult command you had
